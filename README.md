mkdir -p /data2/nfs_server/jenkins

docker-compose up -d --build --force-recreate


remember to modprobe {nfs,nfsd,rpcsec_gss_krb5}

# test mount
sudo /sbin/mount -o vers=3 (hostname -i):/nfs /tmp/whatever

# Troubleshooting

mount.nfs: prog 100003, trying vers=3, prot=17
mount.nfs: portmap query failed: RPC: Program not registered
mount.nfs: requested NFS version or transport protocol is not supported

reboot host
